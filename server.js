var path = require('path')
var bodyParser = require('body-parser')
var express = require('express')
//var webpack = require('webpack')
var https = require('https')
var http = require('http')
var fs = require('fs')
var path = require('path')
var util = require('util')

try {
  var key = fs.readFileSync('/etc/letsencrypt/live/archive.kopunch.club/privkey.pem')
  var cert = fs.readFileSync('/etc/letsencrypt/live/archive.kopunch.club/fullchain.pem')
} catch (e) {
  var nossl = true
}

function EncodeThreadID(mylet, string){
  mylet = parseInt(mylet);
  if(mylet>26)
    string = EncodeThreadID(mylet/26, String.fromCharCode( mylet%26 + 97 )) + string;
  else
    string = String.fromCharCode( mylet%26 + 97 ) + string;
  return string;
}
function DecodeThreadID(mylet) {
  let out = 0
  for (let i = 0; i < mylet.length; ++i) {
    let code = mylet.charCodeAt(mylet.length - i - 1)
    out += (code - 97) * 26 ** i
  }
  return out
}

if (nossl) {
//  var config = require('./webpack.config.dev.js')
} else {
//  var config = require('./webpack.config.js')
}


/*
Loading...

https://facepunch.com/showthread.php?t=1591219 |
      20:43:24 March 17, 2018

Got an HTTP 301 response at crawl time

Redirecting to...

https://forum.facepunch.com/f/general?legacythread=1591219

... -> https://forum.facepunch.com/f/general/bknkr/Forum-Discussion-v6-Newpunch-Migration-Underway/1/

bknkr -> 641801
*/

var app = express()

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())


app.use('/static', express.static('static'))

app.get('/thread/*', (req, res)=>{
  let slug = req.url.substring("/thread/".length)

  let slash = slug.indexOf("/")
  if (slash === -1) slash = undefined;
  let id = slug.substring(0, slash)
  let page = 1

  if (slash !== undefined) {
    slug = slug.substring(slash + 1)
    slash = slug.indexOf("/")
    if (slash === -1) slash = undefined;

    page = Number(slug.substring(0, slash))
  }

  console.log("thread!", id, page)
  if (threads[id]) {
    console.log("found!", threads[id])
    fs.readFile(threads[id], (err, file)=>{
      if (err) return err;
      let json = JSON.parse(file)

      let ret = {}
      ret.archived = json
      ret.id = json.Thread.ThreadId
      ret.title = json.Thread.Name
      ret.iconId = 0
      ret.userId = json.Thread.AuthorUserId
      ret.subforumId = json.Forum.ForumId
      ret.subforumName = json.Forum.Name
      ret.createdAt = json.Thread.CreatedDate
      ret.updatedAt = json.Thread.LastPostDate
      ret.deleted = false
      ret.locked = json.Thread.Closed
      ret.pinned = json.Thread.Sticky
      ret.currentPage = page

      ret.user = []
      ret.user[0] = {}
      ret.user[0].username = json.Posts[0].Username
      ret.user[0].usergroup = 1

      ret.totalPosts = json.Thread.PostCount

      let per_page = json.Page.PerPage
      let start_post = per_page * (page - 1)
      let end_post = per_page * page

      ret.posts = json.Posts.slice(start_post, end_post).map((v, k)=>{
        return {
          id: v.PostId,
          content: v.Message,
          createdAt: v.CreatedDate,
          updatedAt: v.LastEditDate,
          countryName: (v.Country || "").trim(),
          appName: "facepunch.com",
          threadPostNumber: start_post + k,
          user: {
            username: v.Username,
            id: v.UserId,
            usergroup: 1,
          },
          ratings: []
        }
      })

      res.json(ret)
    })
  } else {
    if (loading_archive) {
      res.sendStatus(503)
    } else {
      res.sendStatus(404)
    }
  }
})


if (nossl) {
  var httpServer = http.createServer(app)

  httpServer.listen(8081)

  console.log("Server listening on port 8081")
} else {
  var httpsServer = https.createServer({
    key: key,
    cert: cert,
  }, app)

  httpsServer.listen(8445)

  console.log("Server listening on port 8445")
}

var threads = {}

var archive_root = process.env.ARCHIVE_ROOT || "/mnt/h/FP"
var loading_archive = true
console.log("Indexing archive...")
fs.readdir(archive_root, (err, list)=>{
  if (err) return err;
  let count = 0
  let pending_dirs = list.length
  let pending_files = 0
  const start = process.hrtime.bigint()
  list.forEach((subforum)=>{
    let dir = path.resolve(archive_root, subforum)
    fs.readdir(dir, (err, list)=>{
      if (err) return console.log(err);
      pending_files += list.length
      --pending_dirs
      list.forEach((entry)=>{
        let id = entry.substring(0, entry.indexOf("."))
        threads[id] = path.resolve(dir, entry)
        ++count
        --pending_files
        if (pending_files <= 0) {
          console.log(subforum)
          if (pending_dirs <= 0) {
            const end = process.hrtime.bigint()
            let elapsed = (end - start) / BigInt(1e+6)
            loading_archive = false
            console.log("Indexed", count, "threads in", Number(elapsed) / 1000, "seconds.")
          }
        }
      })
    })
  })
})

